syntax = "proto3";
package pulse.cpm.bind;
option java_package = "com.kitware.pulse.cpm.bind";
option csharp_namespace = "pulse.cpm.bind";
option optimize_for = SPEED;

import "pulse/cdm/bind/Properties.proto";
import "pulse/cdm/bind/Physiology.proto";


message BloodChemistryData
{
  pulse.cdm.bind.BloodChemistrySystemData   Common                                              = 1;
  pulse.cdm.bind.RunningAverageData         ArterialOxygenAverage_mmHg                          = 2;
  pulse.cdm.bind.RunningAverageData         ArterialCarbonDioxideAverage_mmHg                   = 3;
}

message CardiovascularData
{
  pulse.cdm.bind.CardiovascularSystemData   Common                                              = 1;
  bool                                      StartSystole                                        = 2;
  bool                                      HeartFlowDetected                                   = 3;
  bool                                      EnterCardiacArrest                                  = 4;
  double                                    CardiacCyclePeriod_s                                = 5;
  double                                    CurrentCardiacCycleDuration_s                       = 6;
  double                                    LeftHeartElastanceModifier                          = 7;
  double                                    LeftHeartElastance_mmHg_Per_mL                      = 8;
  double                                    LeftHeartElastanceMax_mmHg_Per_mL                   = 9;
  double                                    LeftHeartElastanceMin_mmHg_Per_mL                   = 10;
  double                                    RightHeartElastance_mmHg_Per_mL                     = 11;
  double                                    RightHeartElastanceMax_mmHg_Per_mL                  = 12;
  double                                    RightHeartElastanceMin_mmHg_Per_mL                  = 13;
  
  double                                    CompressionTime_s                                   = 14;
  double                                    CompressionRatio                                    = 15;
  double                                    CompressionPeriod_s                                 = 16;
  
  double                                    CurrentCardiacCycleTime_s                           = 17;
  double                                    CardiacCycleDiastolicVolume_mL                      = 18;
  double                                    CardiacCycleAortaPressureLow_mmHg                   = 19;
  double                                    CardiacCycleAortaPressureHigh_mmHg                  = 20;
  double                                    CardiacCyclePulmonaryArteryPressureLow_mmHg         = 21;
  double                                    CardiacCyclePulmonaryArteryPressureHigh_mmHg        = 22;
  double                                    LastCardiacCycleMeanArterialCO2PartialPressure_mmHg = 23;
  double                                    CardiacCycleStrokeVolume_mL                         = 24;
  
  pulse.cdm.bind.RunningAverageData         CardiacCycleArterialPressure_mmHg                   = 25;
  pulse.cdm.bind.RunningAverageData         CardiacCycleArterialCO2PartialPressure_mmHg         = 26;
  pulse.cdm.bind.RunningAverageData         CardiacCyclePulmonaryCapillariesWedgePressure_mmHg  = 27;
  pulse.cdm.bind.RunningAverageData         CardiacCyclePulmonaryCapillariesFlow_mL_Per_s       = 28;
  pulse.cdm.bind.RunningAverageData         CardiacCyclePulmonaryShuntFlow_mL_Per_s             = 29;
  pulse.cdm.bind.RunningAverageData         CardiacCyclePulmonaryArteryPressure_mmHg            = 30;
  pulse.cdm.bind.RunningAverageData         CardiacCycleCentralVenousPressure_mmHg              = 31;
  pulse.cdm.bind.RunningAverageData         CardiacCycleSkinFlow_mL_Per_s                       = 32;
}

message DrugData
{
  pulse.cdm.bind.DrugSystemData             Common                                              = 1;
}

message EndocrineData
{
  pulse.cdm.bind.EndocrineSystemData        Common                                              = 1;
}

message EnergyData
{
  pulse.cdm.bind.EnergySystemData           Common                                              = 1;
  double                                    EnduranceEnergyStore_J                              = 2;
  double                                    MediumPowerEnergyStore_J                            = 3;
  double                                    PeakPowerEnergyStore_J                              = 4;
  double                                    UsableEnergyStore_J                                 = 5;
  
  pulse.cdm.bind.RunningAverageData         BloodpH                                             = 6;
  pulse.cdm.bind.RunningAverageData         BicarbonateMolarity_mmol_Per_L                      = 7;
}

message GastrointestinalData
{
  pulse.cdm.bind.GastrointestinalSystemData Common                                              = 1;
}

message HepaticData
{
  pulse.cdm.bind.HepaticSystemData          Common                                              = 1;
}

message NervousData
{
  pulse.cdm.bind.NervousSystemData          Common                                              = 1;
  double                                    ArterialOxygenBaseline_mmHg                         = 2;
  double                                    ArterialCarbonDioxideBaseline_mmHg                  = 3;
  bool                                      BaroreceptorFeedbackStatus                          = 4;
  bool                                      BaroreceptorSaturationStatus                        = 5;
  double                                    BaroreceptorActiveTime_s                            = 6;
  double                                    BaroreceptorEffectivenessParameter                  = 7;
  double                                    BaroreceptorMeanArterialPressureBaseline_mmHg       = 8;
  double                                    BaroreceptorSaturationTime_s                        = 9;
  double                                    LastMeanArterialPressure_mmHg                       = 10;
  double                                    PreviousBloodVolume_mL                              = 11;
  double                                    TotalSympatheticFraction                            = 12;
}

message RenalData
{
  pulse.cdm.bind.RenalSystemData           Common                                              = 1;
  bool                                     Urinating                                           = 2;
  double                                   LeftAfferentResistance_mmHg_s_Per_mL                = 3;
  double                                   RightAfferentResistance_mmHg_s_Per_mL               = 4;
  double                                   LeftSodiumFlowSetPoint_mg_Per_s                     = 5;
  double                                   RightSodiumFlowSetPoint_mg_Per_s                    = 6;
  
  pulse.cdm.bind.RunningAverageData        UrineProductionRate_mL_Per_min                      = 7;
  pulse.cdm.bind.RunningAverageData        UrineOsmolarity_mOsm_Per_L                          = 8;
  pulse.cdm.bind.RunningAverageData        SodiumConcentration_mg_Per_mL                       = 9;
  pulse.cdm.bind.RunningAverageData        SodiumExcretionRate_mg_Per_min                      = 10;
  pulse.cdm.bind.RunningAverageData        LeftSodiumFlow_mg_Per_s                             = 11;
  pulse.cdm.bind.RunningAverageData        RightSodiumFlow_mg_Per_s                            = 12;
  pulse.cdm.bind.RunningAverageData        LeftRenalArterialPressure_mmHg                      = 13;
  pulse.cdm.bind.RunningAverageData        RightRenalArterialPressure_mmHg                     = 14;
}

message RespiratoryData
{// Next value : 45
  pulse.cdm.bind.RespiratorySystemData     Common                                              = 1;
  
  bool                                     BreathingCycle                                      = 2;
  bool                                     NotBreathing                                        = 3;
  double                                   TopBreathTotalVolume_L                              = 4;
  double                                   TopBreathAlveoliVolume_L                            = 5;
  double                                   TopBreathPleuralVolume_L                            = 6;
  double                                   TopBreathAlveoliPressure_cmH2O                      = 7;
  double                                   TopBreathDriverPressure_cmH2O                       = 8;
  double                                   TopBreathPleuralPressure_cmH2O                      = 9;
  double                                   LastCardiacCycleBloodPH                             = 10;
  double                                   TopCarinaO2                                         = 11;
  double                                   TopBreathElapsedTime_min                            = 12;
  double                                   BottomBreathElapsedTime_min                         = 13;
  double                                   BottomBreathTotalVolume_L                           = 14;
  double                                   BottomBreathAlveoliVolume_L                         = 15;
  double                                   BottomBreathPleuralVolume_L                         = 16;
  double                                   BottomBreathAlveoliPressure_cmH2O                   = 17;
  double                                   BottomBreathDriverPressure_cmH2O                    = 18;
  double                                   BottomBreathPleuralPressure_cmH2O                   = 19;
  double                                   PeakAlveolarPressure_cmH2O                          = 20;
  double                                   MaximalAlveolarPressure_cmH2O                       = 21;
  pulse.cdm.bind.RunningAverageData        BloodPHRunningAverage                               = 22;
  pulse.cdm.bind.RunningAverageData        MeanAirwayPressure_cmH2O                            = 23;
  
  double                                   ArterialO2PartialPressure_mmHg                      = 24;
  double                                   ArterialCO2PartialPressure_mmHg                     = 25;
  double                                   BreathingCycleTime_s                                = 26;
  double                                   DriverPressure_cmH2O                                = 27;
  double                                   ElapsedBreathingCycleTime_min                       = 28;
  double                                   IERatioScaleFactor                                  = 29;
  double                                   PeakInspiratoryPressure_cmH2O                       = 30;
  double                                   PeakExpiratoryPressure_cmH2O                        = 31;
  double                                   PreviousTargetAlveolarVentilation_L_Per_min         = 32;
  double                                   VentilationFrequency_Per_min                        = 33;
  double                                   VentilationToTidalVolumeSlope                       = 34;
  pulse.cdm.bind.RunningAverageData        ArterialO2RunningAverage_mmHg                       = 35;
  pulse.cdm.bind.RunningAverageData        ArterialCO2RunningAverage_mmHg                      = 36;
  // Muscle Pressure Waveform
  double                                   ExpiratoryHoldFraction                              = 37;
  double                                   ExpiratoryReleaseFraction                           = 38;
  double                                   ExpiratoryRiseFraction                              = 39;
  double                                   InspiratoryHoldFraction                             = 40;
  double                                   InspiratoryReleaseFraction                          = 41;
  double                                   InspiratoryRiseFraction                             = 42;
  double                                   InspiratoryToExpiratoryPauseFraction                = 43;
  
  // Disease States
  double                                   LeftAlveoliDecrease_L                               = 44;
  double                                   RightAlveoliDecrease_L                              = 45;
  
  // Conscious Breathing
  bool                                     ActiveConsciousRespirationCommand                   = 46;
  // Overrides
  double                                   RespiratoryComplianceOverride_L_Per_cmH2O           = 47;
  double                                   RespiratoryResistanceOverride_cmH2O_s_Per_L         = 48;
}

message TissueData
{
  pulse.cdm.bind.TissueSystemData          Common                                              = 1;
  double                                   RestingTissueGlucose_g                              = 2;
  double                                   RestingBloodGlucose_mg_Per_mL                       = 3;
  double                                   RestingBloodLipid_mg_Per_mL                         = 4;
  double                                   RestingBloodInsulin_mg_Per_mL                       = 5;
  double                                   RestingFluidMass_kg                                 = 6;
}